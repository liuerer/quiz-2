package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateStaffRequest;
import com.twuc.webApp.contract.GetStaffResponse;
import com.twuc.webApp.contract.SetZoneIdRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.time.zone.ZoneRulesProvider;
import java.util.*;

@RestController
@RequestMapping("/api")
public class ReservationController {
	
	@Autowired
	private StaffRepository repo;
	
	@PostMapping("/staffs")
	public ResponseEntity createRobUser(@RequestBody @Valid CreateStaffRequest staffRequest){
		if(staffRequest == null){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		Staff staff = new Staff(staffRequest.getFirstName(), staffRequest.getLastName());
		
		Staff saveStaff = repo.save(staff);
		URI uri = URI.create("/api/staff/" + saveStaff.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@GetMapping("/staffs/{staffId}")
	public ResponseEntity getStaffMessage(@PathVariable Long staffId){
		
		Optional<Staff> staff = repo.findById(staffId);
		if(!staff.isPresent()){
			return ResponseEntity.notFound().build();
		}
		GetStaffResponse getStaffResponse = new GetStaffResponse(staff.get());
		
		return ResponseEntity.ok(getStaffResponse);
	}
	
	@GetMapping("/staffs")
	public ResponseEntity getAllStaffMessage(){
		List<Staff> staffs = repo.findAll(new Sort(Sort.Direction.ASC, "id"));
		
		return ResponseEntity.ok(staffs);
	}
	
	@PutMapping("staffs/{staffId}/timezone")
	public ResponseEntity setStaffZoneId(@PathVariable Long staffId,
	                                     @RequestBody @Valid SetZoneIdRequest request){
		Optional<Staff> staff = repo.findById(staffId);
		if(!staff.isPresent()){
			return ResponseEntity.notFound().build();
		}
		
		if(!ZoneRulesProvider.getAvailableZoneIds().contains(request.getZoneId())){
			return ResponseEntity.badRequest().build();
		}
		
		staff.get().setZoneId(request.getZoneId());
		repo.save(staff.get());
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("timezones")
	public ResponseEntity getAllTimeZones(){
		
		Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
		
		ArrayList<String> zoneIds = new ArrayList<>(availableZoneIds);
		Collections.sort(zoneIds);
		
		return ResponseEntity.ok(zoneIds);
		
		
	}
	
	
}
