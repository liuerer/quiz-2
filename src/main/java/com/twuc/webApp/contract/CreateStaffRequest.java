package com.twuc.webApp.contract;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class CreateStaffRequest {
	
	@NotNull
	@Length(max = 64)
	private String firstName;
	
	@NotNull
	@Length(max = 64)
	private String lastName;
	
	public CreateStaffRequest() {
	}
	
	public CreateStaffRequest(@NotNull @Length(max = 64) String firstName, @NotNull @Length(max = 64) String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
