package com.twuc.webApp.contract;

import com.twuc.webApp.web.Staff;

public class GetStaffResponse {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String zoneId;
	
	
	public GetStaffResponse(Staff staff) {
		this.id = staff.getId();
		this.firstName = staff.getFirstName();
		this.lastName = staff.getLastName();
		this.zoneId = staff.getZoneId();
	}
	
	public Long getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getZoneId() {
		return zoneId;
	}
}
