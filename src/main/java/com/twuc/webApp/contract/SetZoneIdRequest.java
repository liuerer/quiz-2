package com.twuc.webApp.contract;

import javax.validation.constraints.NotBlank;

public class SetZoneIdRequest {
	
	@NotBlank
	private String zoneId;
	
	public SetZoneIdRequest() {
	}
	
	public SetZoneIdRequest(@NotBlank String zoneId) {
		this.zoneId = zoneId;
	}
	
	public String getZoneId() {
		return zoneId;
	}
}
