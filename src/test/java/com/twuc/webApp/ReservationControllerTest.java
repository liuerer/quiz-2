package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.CreateStaffRequest;
import com.twuc.webApp.contract.SetZoneIdRequest;
import com.twuc.webApp.web.Staff;
import com.twuc.webApp.web.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReservationControllerTest extends ApiTestBase {
	
	@Autowired
	private StaffRepository staffRepository;
	
	@Test
	void should_get_created_status() throws Exception {
		
		CreateStaffRequest staff = new CreateStaffRequest("Rob", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(staff);
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr))
			.andExpect(status().isCreated())
			.andExpect(header().exists("Location"));
	}
	
	@Test
	void should_get_fail_created_status() throws Exception {
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isBadRequest());
		
		CreateStaffRequest staff = new CreateStaffRequest(null, "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(staff);
		
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr))
			.andExpect(status().isBadRequest());
		
		CreateStaffRequest staff2 = new CreateStaffRequest("Rob", "HaLengthLengthLengthLengthLengthLengthLengthVLengthLengthLengthLengthLengthLength");
		String staffStr2 = new ObjectMapper().writeValueAsString(staff2);
		
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr2))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	void should_create_staff_in_db() throws Exception {
		CreateStaffRequest expectStaff = new CreateStaffRequest("Rob", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(expectStaff);
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr))
			.andExpect(status().isCreated())
			.andExpect(header().exists("Location"));
		
		final Staff staff = staffRepository.findById(1L).orElseThrow(RuntimeException::new);
		assertEquals(expectStaff.getFirstName(), staff.getFirstName());
		assertEquals(expectStaff.getLastName(), staff.getLastName());
	}
	
	@Test
	void should_get_staff_message() throws Exception {
		CreateStaffRequest expectStaff = new CreateStaffRequest("Rob", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(expectStaff);
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr))
			.andExpect(status().isCreated());
		
		mockMvc.perform(get("/api/staffs/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.firstName").value(expectStaff.getFirstName()))
			.andExpect(jsonPath("$.lastName").value(expectStaff.getLastName()));
	}
	
	@Test
	void should_get_404_when_id_is_not_exist() throws Exception {
		mockMvc.perform(get("/api/staffs/1"))
			.andExpect(status().isNotFound());
	}
	
	@Test
	void should_get_all_staffs_message() throws Exception {
		CreateStaffRequest expectStaff = new CreateStaffRequest("Rob", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(expectStaff);
		
		mockMvc.perform(get("/api/staffs"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").isEmpty());
		
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr))
			.andExpect(status().isCreated());
		
		mockMvc.perform(post("/api/staffs")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(staffStr))
			.andExpect(status().isCreated());
		
		mockMvc.perform(get("/api/staffs"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].firstName").value(expectStaff.getFirstName()))
			.andExpect(jsonPath("$[1].firstName").value(expectStaff.getFirstName()));
	}
	
	@Test
	void should_get_all_by_sort() throws Exception {
		CreateStaffRequest expectStaff1 = new CreateStaffRequest("Rob1", "Hall");
		String staffStr1 = new ObjectMapper().writeValueAsString(expectStaff1);
		CreateStaffRequest expectStaff2 = new CreateStaffRequest("Rob2", "Hall");
		String staffStr2 = new ObjectMapper().writeValueAsString(expectStaff2);
		CreateStaffRequest expectStaff3 = new CreateStaffRequest("Rob3", "Hall");
		String staffStr3 = new ObjectMapper().writeValueAsString(expectStaff3);
		
		
		mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(staffStr1));
		mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(staffStr2));
		mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(staffStr3));
		
		mockMvc.perform(get("/api/staffs"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].firstName").value(expectStaff1.getFirstName()))
			.andExpect(jsonPath("$[1].firstName").value(expectStaff2.getFirstName()))
			.andExpect(jsonPath("$[2].firstName").value(expectStaff3.getFirstName()));
	}
	
	@Test
	void should_add_rob_user_time_zone() throws Exception {
		CreateStaffRequest expectStaff = new CreateStaffRequest("Rob1", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(expectStaff);
		mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(staffStr));
		
		mockMvc.perform(put("/api/staffs/1/timezone")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content("{\"zoneId\": \"Asia/Chongqing\"}"))
			.andExpect(status().isOk());
	}
	
	@Test
	void should_return_message_when_request_is_illegal() throws Exception {
		mockMvc.perform(put("/api/staffs/1/timezone")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content("{\"zoneId\": \"Asia/Chongqing\"}"))
			.andExpect(status().isNotFound());
		
		CreateStaffRequest expectStaff = new CreateStaffRequest("Rob1", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(expectStaff);
		mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(staffStr));
		
		mockMvc.perform(put("/api/staffs/1/timezone")
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isBadRequest());
		
		mockMvc.perform(put("/api/staffs/1/timezone")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content("{\"zoneId\": \"asia/chongqing\"}"))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	void should_return_staff_message_with_zone_id() throws Exception {
		CreateStaffRequest expectStaff = new CreateStaffRequest("Rob1", "Hall");
		String staffStr = new ObjectMapper().writeValueAsString(expectStaff);
		mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(staffStr));
		
		mockMvc.perform(get("/api/staffs/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id").value(1))
			.andExpect(jsonPath("$.firstName").value(expectStaff.getFirstName()))
			.andExpect(jsonPath("$.lastName").value(expectStaff.getLastName()))
			.andExpect(jsonPath("$.zoneId").isEmpty());
		
		SetZoneIdRequest expectStaffZoneId = new SetZoneIdRequest("Asia/Chongqing");
		String zoneIdStr = new ObjectMapper().writeValueAsString(expectStaffZoneId);
		
		mockMvc.perform(put("/api/staffs/1/timezone")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(zoneIdStr))
			.andExpect(status().isOk());
		
		mockMvc.perform(get("/api/staffs/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id").value(1))
			.andExpect(jsonPath("$.firstName").value(expectStaff.getFirstName()))
			.andExpect(jsonPath("$.lastName").value(expectStaff.getLastName()))
			.andExpect(jsonPath("$.zoneId").value(expectStaffZoneId.getZoneId()));
	}
	
	@Test
	void should_return_a_timezone_list() throws Exception {
		mockMvc.perform(get("/api/timezones"))
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$[0]").value("Africa/Abidjan"))
			.andExpect(jsonPath("$[1]").value( "Africa/Accra"))
			.andExpect(jsonPath("$[2]").value("Africa/Addis_Ababa"));
	}
}


